variable "server_count" {
  default     = "1"
  description = "The number of WindTrain servers to launch."
}

variable "ssh_key" {
  default = "~/.ssh/keys/scaleway/scaleway"
  description = "Path to private ssh key"
}

variable "server_type" {
  default = "VC1S"
  description = "Server type. E.g. VC1S, C2M (x86_64 only)"
}

variable "image_tag" {
  default = "6df30815a65189219a650187c4afffe2f156b5d6"
  description = "Tag of the whiskeypipe image"
}

provider "scaleway" {
  region       = "ams1"
}

resource "scaleway_ip" "ip" {
  server = "${scaleway_server.windtrain.id}"
}

data "scaleway_image" "ubuntu" {
  architecture = "x86_64"
  name         = "Ubuntu Xenial"
}

resource "scaleway_server" "windtrain" {
  count = "${var.server_count}"
  name  = "dds1-platform-${count.index + 1}"
  image = "${data.scaleway_image.ubuntu.id}"
  type  = "${var.server_type}"
  security_group = "${scaleway_security_group.http.id}"
  dynamic_ip_required = true

  connection {
    type         = "ssh"
    user         = "root"
    agent        = true
    private_key = "${file("${var.ssh_key}")}"
  }

  provisioner "remote-exec" {
    inline = [
      "wget -qO- https://get.docker.com/ | sh",
      "curl -L https://github.com/docker/compose/releases/download/1.13.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose",
      "sudo chmod +x /usr/local/bin/docker-compose"
    ]
  }

  provisioner "remote-exec" {
    inline = [
      "docker run -d -p 80:80 --name whiskeypipe registry.gitlab.com/winderresearch/whiskeypipe:${var.image_tag}",
    ]
  }
}

resource "scaleway_security_group" "http" {
  name        = "http"
  description = "allow HTTP and HTTPS traffic"
}

resource "scaleway_security_group_rule" "http_accept" {
  security_group = "${scaleway_security_group.http.id}"

  action    = "accept"
  direction = "inbound"
  ip_range  = "0.0.0.0/0"
  protocol  = "TCP"
  port      = 80
}

output "ip" {
  value = "${scaleway_ip.ip.ip}"
}

output "address" {
  value = "http://${scaleway_ip.ip.server}.pub.cloud.scaleway.com"
}
