from recommender import Recommender


def test_phils_favourite():
    r = Recommender()
    w = r.reccomend()
    assert len(w) > 1
    assert w.isin(["Ardbeg"]).any()


def test_another():
    r = Recommender()
    w = r.reccomend("Macallan")
    assert w.isin(["Glendronach"]).any()


def test_not_in_set():
    r = Recommender()
    w = r.reccomend("not there")
    assert len(w) == 0